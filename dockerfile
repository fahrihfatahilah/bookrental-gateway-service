FROM openjdk:17

WORKDIR /app

COPY target/gatewayservice-0.0.1-SNAPSHOT.jar /app/gatewayservice-0.0.1.jar

EXPOSE 8083

CMD ["java", "-jar", "/app/gatewayservice-0.0.1.jar"]