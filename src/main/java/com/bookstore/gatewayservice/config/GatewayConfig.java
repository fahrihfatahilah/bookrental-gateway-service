package com.bookstore.gatewayservice.config;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
@Configuration

public class GatewayConfig {
    @Value("${url.book.service}")
    String urlBookService;

    @Value("${url.member.service}")
    String urlMemberServices;

    @Value("${url.peminjaman.service}")
    String urlPeminjamanService;

    private static final Logger log = LoggerFactory.getLogger(GatewayConfig.class);

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("book-service", r -> r.path("/api/books/**")
                        .uri(urlBookService))
                .route("member-service", r -> r.path("/api/members/**")
                        .uri(urlMemberServices))
                .route("peminjaman-service", r -> r.path("/api/peminjaman/**")
                        .uri(urlPeminjamanService))
                .build();
    }
}

